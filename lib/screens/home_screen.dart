import 'package:flutter/material.dart';
import 'package:m3_app/screens/profile_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  icon: const Icon(Icons.menu, color: Colors.grey, size: 36),
                  color: Colors.white,
                  onPressed: () {},
                ),
                IconButton(
                  icon: const Icon(Icons.person_outline_rounded,
                      color: Colors.grey, size: 36),
                  color: Colors.white,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProfileScreen()));
                  },
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text(
              "Dashboard Options",
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
              textAlign: TextAlign.start,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Center(
              child: Wrap(
                spacing: 20.0,
                runSpacing: 20.0,
                children: [
                  //box 1
                  SizedBox(
                    width: 160.0,
                    height: 160.0,
                    child: Card(
                      color: Colors.grey[500],
                      elevation: 2.2,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            children: [
                              Icon(Icons.search, color: Colors.white, size: 65),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "Find",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 19),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  //box 2
                  SizedBox(
                    width: 160.0,
                    height: 160.0,
                    child: Card(
                      color: Colors.purple[500],
                      elevation: 2.2,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            children: [
                              Icon(Icons.account_balance_wallet,
                                  color: Colors.white, size: 65),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "Credits",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 19),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),

                  //box 3

                  SizedBox(
                    width: 160.0,
                    height: 160.0,
                    child: Card(
                      color: Colors.purple[500],
                      elevation: 2.2,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            children: [
                              Icon(Icons.home, color: Colors.white, size: 65),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "Houses",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 19),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  //box 4
                  SizedBox(
                    width: 160.0,
                    height: 160.0,
                    child: Card(
                      color: Colors.grey[500],
                      elevation: 2.2,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            children: [
                              Icon(Icons.settings,
                                  color: Colors.white, size: 65),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "Settings",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 19),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  //end boxes
                ],
              ),
            ),
          )
        ],
      )),
    );
  }
}
